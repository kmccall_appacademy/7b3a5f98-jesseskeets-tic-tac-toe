class Board

attr_reader :grid
  def initialize grid = Array.new(3) { Array.new(3) }
    @grid = grid
  end

  def place_mark(pos, mark)
    # row, col = pos        <- shorthand notation
    row = pos[0]
    col = pos[1]
    grid[row][col] = mark
  end

  def empty?(pos)
    row = pos[0]
    col = pos[1]
    grid[row][col] == nil
  end

  def winner

    #Row Condition
  @grid.each do |row|
    if row.all? {|el| el == row.first}
      return row.first
    end
  end



  #  DIAG Condition
   if rightdiag || leftdiag
     return grid[1][1]
   end

   #Column Condition
   columns.each do |col|
     if col.compact.uniq.length == 1
       return col.first
     end
   end
   return nil
  end

  def columns
    col = 0
    columns = []
    while col < 3
      column = []
      column << grid[0][col]
      column << grid[1][col]
      column << grid[2][col]
      col += 1
      columns << column
    end
    columns
  end

  def leftdiag
    ldiag = []
    ldiag << grid[0][0]
    ldiag << grid[1][1]
    ldiag << grid[2][2]
    ldiag.compact.size == 3 && ldiag.compact.uniq.length == 1
    # [grid[0][0],grid[1][1],grid[2][2]] <- would also work
  end

  def rightdiag
    rdiag = []
    rdiag << grid[0][2]
    rdiag << grid[1][1]
    rdiag << grid[2][0]
    rdiag.compact.size == 3 && rdiag.compact.uniq.length == 1
  end

  def row
    i = 0
    while i < 3
       return grid[i] if grid[i].compact.uniq.length == 1
      i += 1
    end
  end

  def over?
    winner || grid.flatten.compact.size == 9
  end
  def winning_moves(mark)
    wins = []
    length = @grid.size-1
    (0..length).each do |arr|
      (0..length).each do |pos|
        if @grid[arr][pos].nil?
          place_mark([arr,pos], mark)
          wins << [arr,pos] if winner == mark
          @grid[arr][pos] = nil
        end
      end
    end
    wins
  end

  def valid_move
    moves = []
    length = @grid.size-1
    (0..length).each do |arr|
      (0..length).each do |pos|
        if @grid[arr][pos].nil?
          moves << [arr,pos]
        end
      end
    end
    moves
  end
end

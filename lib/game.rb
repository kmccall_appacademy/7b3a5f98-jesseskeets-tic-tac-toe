require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :player_one, :player_two, :current_player, :board
  attr_accessor :mark

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  def play_turn
    move = current_player.get_move
    @board.place_mark(move,current_player.mark)
    switch_players!
  end

  def switch_players!
    if @current_player == player_one
      @current_player = player_two
    else
      @current_player = player_one
    end
  end

end

# game.new(Player.new)

class HumanPlayer

  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
    @mark = :X
  end

  def get_move
    p "Where would you like to place your move"
    move = gets.chomp
    pos = move.split(",").map(&:to_i)
  end

  def display(board)
    p board
  end

end
